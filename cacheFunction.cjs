function cacheFunction(cb) {

    if (typeof cb !== 'function' || arguments.length < 1) {
        throw new Error("There seems to be an error!");
    }

    let cache = {};

    return function (...args) {
        if (cache.hasOwnProperty(args)) {

            return cache[args];
        }
        else {
            let result = cb(...args);
            cache[args] = result

            return result;
        }
    }
}

module.exports = cacheFunction;