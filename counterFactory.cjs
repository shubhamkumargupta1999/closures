function counterFactory() {
    let count = 0;

    function increment() {
        count++;

        return count;
    }

    function decrement() {
        count--;

        return count;
    }

    return { increment, decrement };
}

module.exports = counterFactory;