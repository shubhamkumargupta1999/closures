function limitFunctionCallCount(cb, n) {

    if (typeof cb !== 'function'
        || typeof n !== 'number'
        || arguments.length < 2) {
        throw new Error("There seems to be an error!");
    }

    let count = 0;

    function limitedCb(...args) {
        if (count < n) {
            count++;

            return cb(...args);
        } else {
            return null;
        }
    }

    return limitedCb;
}

function cb(a,b){
    return a+b;
}

module.exports = limitFunctionCallCount;