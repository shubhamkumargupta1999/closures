const counterFactory = require('../counterFactory.cjs')

test("testing counterFactory", () => {

    let counter = counterFactory()

    expect(counter.increment()).toBe(1);
    expect(counter.decrement()).toBe(0);

})
