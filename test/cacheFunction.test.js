const cacheFunction = require('../cacheFunction.cjs')

function cb(a, b) {
    return a + b;

}

test("testing cacheFunction", () => {

    const memo = cacheFunction(cb);

    expect(memo(3,5)).toBe(8);

})