const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')

function cb(a, b) {
    return a + b;
}

test("test limitFunctionCallCount", () => {

    const a = limitFunctionCallCount(cb, 1);
    a(3, 4);

    expect(a(3, 4)).toStrictEqual(null);
});
